<?php

header('Content-Type: application/json');

$conn    = mysqli_connect('localhost', 'root', '', 'vue_todolist');
$request = $_SERVER['REQUEST_METHOD']; // GET, PATCH, DELETE, POST, PUT 

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

switch ($request) {
    case 'GET': /** Menampilkan semua record */
        $sql = "SELECT * FROM todo";
        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) > 0) { // cek ada data atau tidak
            echo json_encode(mysqli_fetch_all($result, MYSQLI_ASSOC));
            http_response_code(200);
            die();
        } else {
            http_response_code(400);
            die();
        }

        break;
    case 'POST': /** Membuat to-do-list */
        $text = $_POST['text'];
        $done = $_POST['done'];
        $date = $_POST['date'];

        if (!isset($text)) {
            http_response_code(400);
            die();
        } else {
            $sql = "INSERT INTO todo (text, done, id_date) VALUES ('$text', $done, '$date')";
            $result = mysqli_query($conn, $sql);
            http_response_code(200);
            die();
        }

        break;
    case 'DELETE': /** Menghapus to-do-list */
        $id_date = $_GET['id_date'];
        $sql = "DELETE FROM todo WHERE id_date = '$id_date'";
        $result = mysqli_query($conn, $sql);
        http_response_code(200);
        die();

        break;
    case 'PATCH': /** Mengedit boolean pada done */
        $id_date = $_GET['id'];
        $done    = $_GET['done'];

        $sql = "UPDATE todo SET done=$done WHERE id_date = '$id_date'";
        $result = mysqli_query($conn, $sql);
        http_response_code(200);
        die();

        break;
    default: 
        break; 
}