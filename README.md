## Buat database
id (auto-increment)
text (string)
done (boolean)
id_date (string)

## Fitur-fitur
1. GET     => Menampilkan semua to-do-list
2. POST    => Membuat to-do-list
3. DELETE  => Menghapus to-do-list
4. PATCH   => Mengedit done dengan memainkan booleannya
5. Mengirim data dalam bentuk JSON dan menerima data dalam bentuk JSON (Convert  ke JSON)

## Teknologi
1. Vue.js versi 1.0.25
2. Vue Resource versi 0.9.0 => untuk berinteraksi dengan HTTP Client / (AJAX)
   https://github.com/pagekit/vue-resource